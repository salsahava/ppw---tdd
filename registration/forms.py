from django import forms

class UserForm(forms.Form):
	username = forms.CharField(label='Username', widget=forms.TextInput(attrs={
				"class" : "fields",
				"required" : True,
				"onInput" : "validateUserInfo()",
				"placeHolder" : "Enter your username"
		}), max_length=40)

	email = forms.EmailField(label='Email', widget=forms.EmailInput(attrs={
			"class" : "fields",
			"required" : True,
			"onInput" : "validateUserInfo()",
			"placeHolder" : "Enter your email"
		}), max_length=40)

	password = forms.CharField(label='Password', widget=forms.PasswordInput(attrs={
			"class" : "fields",
			"required" : True,
			"onInput" : "validateUserInfo()",
			"placeHolder" : "Enter your password"
		}), max_length=40)

	password_confirmation = forms.CharField(label='Confirm Password', widget=forms.PasswordInput(attrs={
			"class" : "fields",
			"required" : True,
			"onInput" : "validateUserInfo()",
			"placeHolder" : "Enter your password again"
	}), max_length=40)