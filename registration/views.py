from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from .forms import UserForm
from .models import User
import json

def index(request):
	form = UserForm()
	response = {"form" : form}
	return render(request, 'registration.html', response)

def check_user(request):
	if request.method == "POST" and request.is_ajax():
		json_data = json.loads(request.body, encoding='UTF-8')
		existing_user = User.objects.filter(email=json_data['email']).first()
		form = UserForm({
			'username' : json_data['username'],
			'email' : json_data['email'],
			'password' : json_data['password'],
			'password_confirmation' : json_data['password_confirmation']
			})
		password_confirmed = json_data['password'] == json_data['password_confirmation']
		return JsonResponse({
			"valid"	: form.is_valid(),
			"pwd_confirmed" : password_confirmed,
			"existing_user" : existing_user != None
			})
	return HttpResponse("User already existed", status=405)

def create_user(request):
	if request.method == "POST" and request.is_ajax():
		json_data = json.loads(request.body, encoding='UTF-8')
		User.objects.create(
			username = json_data['username'],
			email = json_data['email'],
			password = json_data['password']
		)
		return HttpResponse("New user successfully added", status=201)
	else:
		return HttpResponse("User already existed", status=405)

def show_subscribers(request):
	subscribers = User.objects.all()
	print(subscribers)
	listOfSubs = []

	for sub in subscribers:
		data = {'username' : sub.username, 'email' : sub.email}
		listOfSubs.append(data)

	return JsonResponse(listOfSubs, safe=False)

def deleteUser(request):
    if (request.method == "POST"):
        email = request.POST['email']
        User.objects.get(email=email).delete()
    return HttpResponse("User deleted")


