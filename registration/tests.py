from django.test import TestCase
from django.apps import apps
from django.urls import resolve
from django.test import Client

from .apps import RegistrationConfig
from .models import User
from .views import index, create_user, check_user

import json
import requests

class RegistAppTest(TestCase):
	def test_apps_config(self):
		self.assertEqual(RegistrationConfig.name, 'registration')
		self.assertEqual(apps.get_app_config('registration').name, 'registration')

class UserRegistTest(TestCase):
	def test_registration_page_exists(self):
		response = Client().get('/registration/')
		self.assertEqual(response.status_code, 200)

	def tes_regist_page_using_index_func(self):
		found = resolve('/registration/')
		self.assertEqual(found.func, index)

	def test_regist_page_using_correct_template(self):
		response = Client().get('/registration/')
		self.assertTemplateUsed(response, 'registration.html')

class UserModelTest(TestCase):
	def test_default_attr(self):
		user = User()
		self.assertEqual(user.username, '')
		self.assertEqual(user.email, '')
		self.assertEqual(user.password, '')

	def test_user_verbose_name_plural(self):
		self.assertEqual(str(User._meta.verbose_name_plural), "users")

class UserCheckingTest(TestCase):
	def test_post_ajax_request_to_check_user(self):
		response = Client().post(
			'/registration/check_user/',
			json.dumps({
				'username' : 'username',
				'email' : 'email@email.com',
				'password' : 'password',
				'password_confirmation' : 'password'
			}),
			content_type="application/json",
			HTTP_X_REQUESTED_WITH='XMLHttpRequest'
		)

		data = response.json()
		self.assertTrue(data['valid'])
		self.assertTrue(data['pwd_confirmed'])
		self.assertFalse(data['existing_user'])

	def test_get_ajax_request_to_check_user(self):
		response = Client().get('/registration/check_user/')
		self.assertEqual(response.status_code, 405)

class UserCreationTest(TestCase):
	def test_post_ajax_request_to_create_user(self):
		response = Client().post(
			'/registration/create_user/',
			json.dumps({
				'username' : 'username',
				'email' : 'email@email.com',
				'password' : 'password'
			}),
			content_type="application/json",
			HTTP_X_REQUESTED_WITH='XMLHttpRequest'
		)
		self.assertEqual(response.status_code, 201)

	def test_get_ajax_request_to_create_user(self):
		response = Client().get('/registration/create_user/')
		self.assertEqual(response.status_code, 405)