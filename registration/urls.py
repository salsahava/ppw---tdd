from django.conf.urls import url
from .views import *

urlpatterns = [
	url(r'^$', index, name='index'),
	url(r'^check_user/', check_user, name='check_user'),
	url(r'^create_user/', create_user, name='create_user'),
	url(r'^show_subscribers/', show_subscribers, name='show_subscribers'),
	url(r'^deleteUser/', deleteUser, name='deleteUser'),
]