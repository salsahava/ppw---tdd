from django.db import models

class Status(models.Model):
    status = models.TextField(max_length=300)
    date_posted = models.DateTimeField(auto_now_add=True, editable=False, null=False, blank=False)

class Book(models.Model):
	book_title = models.CharField(max_length=100)
	book_description = models.TextField()
	created_date = models.DateTimeField(auto_now_add=True)