from django.contrib import admin
from .models import Status, Book

# Register your models here.
admin.site.register(Status)
admin.site.register(Book)

