from django.test import TestCase
from django.test import Client
from django.urls import resolve

from .views import landing_page, addStatus, profile
from .models import Status
from .forms import Status_Form

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

class StatusFormUnitTest(TestCase):
	def test_story6_url_exists(self):
		response = Client().get('/statusform/')
		self.assertEqual(response.status_code, 200)

	def test_template_story6(self):
		response = Client().get('/statusform/')
		self.assertTemplateUsed(response, 'landing_page.html')

	def test_story6_using_landingpage_func(self):
		found = resolve('/statusform/')
		self.assertEqual(found.func, landing_page)

	def test_story6_hello(self):
		response = Client().get('/statusform/')
		test = "Hello, Apa Kabar?"
		html_response = response.content.decode('utf8')
		self.assertIn(test, html_response)

	def test_model_can_create_new_status(self):
		new_status = Status.objects.create(status='Test')
		counting_all_available_status = Status.objects.all().count()
		self.assertEqual(counting_all_available_status, 1)

	def test_profile_url_exists(self):
		response = Client().get('/statusform/profile/')
		self.assertEqual(response.status_code, 200)

	def test_template_profile(self):
		response = Client().get('/statusform/profile/')
		self.assertTemplateUsed(response, 'profile.html')

	def test_bookrecs_url_exists(self):
		response = Client().get('/statusform/bookrecs/')
		self.assertEqual(response.status_code, 200)

	def test_template_bookrecs(self):
		response = Client().get('/statusform/bookrecs/')
		self.assertTemplateUsed(response, 'bookrecs.html')

	# def test_can_get_JSON(self):
	# 	response = Client().get('/getJSON/')
	# 	self.assertEqual(response.status_code, 200)
                
        
# class StatusFormFuncTest(TestCase):
# 	def setUp(self):
# 		chrome_options = Options()
# 		chrome_options.add_argument('--dns-prefetch-disable')
# 		chrome_options.add_argument('--no-sandbox')
# 		chrome_options.add_argument('--headless')
# 		chrome_options.add_argument('disable-gpu')

# 		service_log_path = "./chromedriver.log"
# 		service_args = ['--verbose']

# 		self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
# 		self.browser.implicitly_wait(25)

# 		super(StatusFormFuncTest, self).setUp()

# 	def test_can_post_status(self):
# 		self.browser.get('http://127.0.0.1:8000/')
# 		time.sleep(3)
# 		status_box = self.browser.find_element_by_name('status')
# 		status_box.send_keys('Coba Coba')
# 		time.sleep(3)
# 		status_box.submit()
# 		time.sleep(5)

# 		self.assertIn('Coba Coba', self.browser.page_source)

# 	def test_profile_layout(self):
# 		self.browser.get('http://127.0.0.1:8000/statusform/profile/')

# 		self.assertIn('salsahava\'s status', self.browser.title)

# 		# header_text = self.browser.find_element_by_tag_name('h1').text
# 		# self.assertIn('Hello, Apa Kabar?', header_text)

# 	def test_profile_css(self):
# 		self.browser.get('http://127.0.0.1:8000/statusform/profile/')

# 		bg_color = self.browser.find_element_by_class_name('profile').value_of_css_property('background')
# 		self.assertEqual(bg_color, 'rgb(8, 150, 161) none repeat scroll 0% 0% / auto padding-box border-box')

# 		text_color = self.browser.find_element_by_class_name('answ').value_of_css_property('color')
# 		self.assertEqual(text_color, 'rgba(255, 255, 255, 1)')

# 	def tearDown(self):
# 		self.browser.implicitly_wait(3)
# 		self.browser.quit()
# 		super(StatusFormFuncTest, self).tearDown()
