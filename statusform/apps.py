from django.apps import AppConfig


class StatusformConfig(AppConfig):
    name = 'statusform'
