from django.conf.urls import url
from .views import landing_page
from .views import addStatus
from .views import profile
from .views import bookrecs
from .views import getJSON
from .views import login
from .views import logout

urlpatterns = [
    url(r'^$', landing_page, name='landing_page'),
    url(r'^addstatus', addStatus, name='addStatus'),
    url(r'^profile/', profile, name='profile'),
    url(r'^bookrecs/', bookrecs, name='bookrecs'),
    url(r'^getJSON/', getJSON, name='getJSON'),
    url(r'^login/', login, name='login'),
    url(r'^logout/', logout, name='logout'),
]
