from django.http import JsonResponse
from django.shortcuts import render, redirect
from .forms import Status_Form
from .models import Status, Book
from django.http import HttpResponseRedirect
from django.urls import reverse
import  requests

response = {}

def landing_page(request):
    response['author'] = "Salsabila Hava Qabita"
    status = Status.objects.all()
    response['status'] = status
    html = 'landing_page.html'
    response['status_form'] = Status_Form
    return render(request, html, response)

def addStatus(request):
    form = Status_Form(request.POST or None)
    if (request.method == 'POST' and form.is_valid()):
        response['status'] = request.POST['status']

        status = Status(status=response['status'])

        status.save()
        return HttpResponseRedirect('/statusform/')
    else:
        return HttpResponseRedirect('/statusform/')

def profile(request):
    return render(request, "profile.html")

def bookrecs(request):
    book = Book.objects.all()
    response['book'] = book
    return render(request, "bookrecs.html", response)

def getJSON(request):
    txt = request.GET.get('q', '')
    bookAPI_URL = "https://www.googleapis.com/books/v1/volumes?q=" + txt

    get = requests.get(bookAPI_URL).json()
    result = get['items']
    # newResult = []

    # for items in result:
    #     idData = items['id']
    #     books = {"title": items['volumeInfo']['title'], "authors": items['volumeInfo']['authors'], 
    #             "publisher": items['volumeInfo']['publisher'], "publishedDate": items['volumeInfo']['publishedDate'], 
    #             "image": items['volumeInfo']['imageLinks']['thumbnail'], "description": items['volumeInfo']['description'], 
    #             "id": items['id']}
    #     newResult.append(books)

    return JsonResponse({'data': result})

def login(request):
    if  request.method == 'POST':
        try:
            token = request.POST['id_token']
            id_info = id_token.verify_oauth2_token(token, requests.Request(), '210851441969-2birvgql98h07bg5p19c3bubfaui63gi.apps.googleusercontent.com')

            if id_info['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
                raise ValueError('Wrong issuer.')

            user_id = id_info['sub']
            name = id_info['name']
            email = id_info['email']
            request.session['user_id'] = user_id
            request.session['name'] = name
            request.session['email'] = email
            request.session['books'] = []

            return JsonResponse({'status': '0', 'url': reverse('landing_page')})
        except ValueError:
            return JsonResponse({'status': '1'})
    else:
        return render(request, 'login.html')

def logout(request):
    request.session.flush()
    return HttpResponseRedirect(reverse('login'))