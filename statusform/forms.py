from django import forms

class Status_Form(forms.Form):
    error_messages = {
        'required': 'This field is required.',
        'invalid': 'Please fill this field with the right type of input',
    }

    status_attrs = {
        'type': 'text',
        'class': 'status-form-textarea'
    }

    status = forms.CharField(label='Status', required=True, widget=forms.TextInput(attrs=status_attrs))
