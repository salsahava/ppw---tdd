function validateUserInfo() {
	event.preventDefault(true);
	let fields = document.forms["user_form"].getElementsByTagName("input");
	let csrf_token_entry = fields[0].value;
	let username_entry = fields[1].value;
	let email_entry = fields[2].value;
	let password_entry = fields[3].value;
	let password_confirmation_entry = fields[4].value;

	$.ajax({
		url: '/registration/check_user/',
		type: 'POST',
		dataType: 'json',
		data: JSON.stringify({
			username : username_entry,
			email : email_entry,
			password : password_entry,
			password_confirmation : password_confirmation_entry
		}),
		beforeSend: function(request) {
			request.setRequestHeader('X-CSRFToken', csrf_token_entry);
		},
		success: function(result) {
			let feedback = document.getElementById("feedback");
			let button = document.getElementById("submit-button");
			feedback.classList.remove("alert-text", "blue-text");
			if (result['valid'] && result['pwd_confirmed']) {
				if (result['existing_user']) {
					button.disabled = true;
					feedback.classList.add("alert-text");
					feedback.innerText = "Email taken";
				} else {
					button.disabled = false;
					feedback.classList.add("blue-text");
					feedback.innerText = "All user info available";
				}
			} else if (!result['valid']) {
				button.disabled = true;
				feedback.classList.add("alert-text");
				feedback.innerText = "Your entry is not valid";
			} else if (!result['pwd_confirmed']) {
				button.disabled = true;
				feedback.classList.add("alert-text");
				feedback.innerText = "Your password confirmation doesn't match";
			}
		}
	});
}

function createUser() {
	event.preventDefault(true);
	let form = document.forms["user_form"];
	let fields = form.getElementsByTagName("input");
	let csrf_token_entry = fields[0].value;
	let username_entry = fields[1].value;
	let email_entry = fields[2].value;
	let password_entry = fields[3].value;
	
	$.ajax({
		url: '/registration/create_user/',
		type: 'POST',
		dataType: 'json',
		data: JSON.stringify({
			username : username_entry,
			email : email_entry,
			password : password_entry
		}),
		beforeSend: function(request) {
			request.setRequestHeader('X-CSRFToken', csrf_token_entry);
		},
		complete: function(result) {
			let feedback = document.getElementById("feedback");
			feedback.classList.remove("alert-text", "blue-text");
			feedback.classList.add("blue-text");
			feedback.innerHTML = "Your account has been added successfully!";
			
			let button = document.getElementById("submit-button");
			button.disabled = true;
			form.reset();
		}
	});
}