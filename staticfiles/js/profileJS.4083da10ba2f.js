
$(function() {
	$("#accordion").accordion({
		collapsible: true
		heightStyle: "content"
    });
});
  		
$(function() {
	$("button").button();

	if ($(".profile").css("background") == "#0896A1") {
		$("button").click(function() {
			$(".profile").css("background", "pink"),
			$("h1, .q").css("color", "#A10848"),
			$("#accordion h3").css("background-color", "#A10848")
		});
	}

	if ($(".profile").css("background") == "pink") {
		$("button").dblclick(function() {
			$(".profile").css("background", "#0896A1"),
			$("h1, .q").css("color", "#343F4B"),
			$("#accordion h3").css("background-color", "#343F4B"),
		});
	}
});