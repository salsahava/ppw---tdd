$(function() {
	$("#accordion").accordion({
		collapsible: true
    });
});
  		
$(function() {
	$("button").button();
	$("button").click(function() {
		$(".profile").css("background", "pink"),
		$("h1, .q").css("color", "#A10848"),
		$("#accordion h3").css("background-color", "#A10848")
	});
});

$( function() {
    var progressbar = $( "#progressbar" ),
      progressLabel = $( ".progress-label" );
 
    progressbar.progressbar({
      value: false,
      change: function() {
        progressLabel.text( progressbar.progressbar( "value" ) + "%" );
      },
      complete: function() {
        progressLabel.text( "Complete!" );
      }
    });
 
    function progress() {
      var val = progressbar.progressbar( "value" ) || 0;
 
      progressbar.progressbar( "value", val + 2 );
 
      if ( val < 99 ) {
        setTimeout( progress, 80 );
      }
    }
 
    setTimeout( progress, 2000 );
});