$(function() {
	$("#accordion").accordion({
		collapsible: true
    });
});
  		
$(function() {
	$("#change").click(function() {
		$(".profile").css("background", "pink"),
		$("h1, .q").css("color", "#A10848"),
		$("#accordion h3").css("background-color", "#A10848")
	});
});

var myVar;

function myFunction() {
    myVar = setTimeout(showPage, 500);
}

function showPage() {
  document.getElementById("loader").style.display = "none";
  document.getElementById("myDiv").style.display = "block";
}


/* Table */
$(document).ready(function(){
	var urlParam = new URLSearchParams(window.location.search);
	$.ajax({
		method: "GET",
		url:  "/getJSON/?q=" + urlParam.get('q'),
		success: function(result) {
			var bookList = result.data;

			for (i = 0; i < bookList.length; i++) {
				var title = bookList[i].title;
				// var button = '<button class="btn_fav btn btn-outline-light" id="' + bookList[i].id + 
				// 				'" onclick="addFavorite('+ '\'' + bookList[i].id + '\'' + ')">' + 
				// 				'<i class="fa fa-star-o" style="color:yellow; font-size:20px">Add to Favorite</i></a>';

				var button = '<button class="btn_fav btn btn-outline-light" onclick="addFavorite()">' + 
							'<i class="fa fa-star-o" style="color:yellow; font-size:20px">Add to Favorite</i></a>';
				var table = '<tr>' + 
								'<td>' + '<img src="' + bookList[i].image + '">' + '</td>' + 
								'<td>' + title + '</td>' + 
								'<td>' + bookList[i].authors + '</td>' + 
								'<td>' + bookList[i].publisher + '</td>' + 
								'<td>' + bookList[i].publishedDate + '</td>' + 
								'<td>' + bookList[i].description + '</td>' + 
								'<td>' + button + '</td>' + 
							'</tr>';

				$("#books").append(table);
			}
		},

		error: function(error) {
			alert("Book not found")
		}
	});
});


/*Search Bar*/
$("input").on("keyup", function(e){
	q = e.currentTarget.value
	console.log(q)

	$.ajax({
		method: "GET",
		url: "/getJSON/?q=" + urlParam.get('q'),
		success: function(result) {
			var bookList = result.data;

			for (i = 0; i < bookList.length; i++) {
				var title = bookList[i].title;

				var button = '<button class="btn_fav btn btn-outline-light" onclick="addFavorite()">' + 
							'<i class="fa fa-star-o" style="color:yellow; font-size:20px">Add to Favorite</i></a>';
				var table = '<tr>' + 
								'<td>' + '<img src="' + bookList[i].image + '">' + '</td>' + 
								'<td>' + title + '</td>' + 
								'<td>' + bookList[i].authors + '</td>' + 
								'<td>' + bookList[i].publisher + '</td>' + 
								'<td>' + bookList[i].publishedDate + '</td>' + 
								'<td>' + bookList[i].description + '</td>' + 
								'<td>' + button + '</td>' + 
							'</tr>';

				$("#books").append(table);
			}
		},

		error: function(error) {
			alert("Book not found")
		}
	});
});

function addFavorite() {
	counter++;
	document.getElementById("counter").innerHTML = counter + " Favorite Books";

	// var star = $('#'+id).html();

	// if (star.includes("yellow")) {
	// 	counter++;
	// 	$('#'+id).html("<i class='fa fa-star' style='color:yellow'></i>");
	// 	$(".counter-fav").html("<i class='fa fa-star' style='color:yellow; font-size:20px'>" + counter + " Favorite Books</i>");
	// }
	// else {
	// 	counter--;
	// }
};
